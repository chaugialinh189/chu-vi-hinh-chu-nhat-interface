﻿// See https://aka.ms/new-console-template for more information
public class CirCle : Shape<float>
{
    private float PI = 3.14f;
    private int R;
    public CirCle(int _R)
    {

        bool check = true;
        do
        {
            try
            {
                R = _R;
                if (R < 0)
                {
                    check = false;
                    throw new ArgumentException();
                }
            }
            catch (Exception e)
            {
                check = false;
                Console.WriteLine("Lam On chi nhap so nguyen duong");
                R = int.Parse(Console.ReadLine());
            }
        } while (check == false);
    }
    public override float CalculateArea()
    {
        return PI * R * R;
    }

    public override float CalculatePerimeter()
    {
        return 2 * PI * R;
    }
}
