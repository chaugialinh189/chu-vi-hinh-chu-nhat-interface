﻿public class Triangle : Shape<int>
{
    private int a;
    private int b;
    private int c;
    public Triangle(int _a, int _b, int _c)
    {
        bool check = true;
        do
        {
            try
            {
                a = _a;
                b = _b;
                c = _c;
                if (a + b < c || a + c < b || b + c < a)
                {
                    check = false;
                    throw new ArgumentException();
                }
            }
            catch (Exception e)
            {
                check = false;
                Console.WriteLine("Ban chua nhap dung tam giac");
                a = int.Parse(Console.ReadLine());
                b = int.Parse(Console.ReadLine());
                c = int.Parse(Console.ReadLine());
            }
        } while (check == false);
    }
    public override int CalculateArea()
    {
        int p = (a + b + c) / 2;
        return (int)Math.Sqrt(p * (p - a) * (p - c) * (p - b));
    }

    public override int CalculatePerimeter()
    {
        return a + b + c;
    }
}
