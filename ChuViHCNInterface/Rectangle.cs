﻿// See https://aka.ms/new-console-template for more information
public class Rectangle : Shape<int>
{
    private int ChieuDai;
    private int ChieuRong;
    public Rectangle(int _ChieuDai, int _ChieuRong)
    {
        bool check = true;
        do
        {
            try
            {
                ChieuDai = _ChieuDai;
                ChieuRong = _ChieuRong;
                if (ChieuDai < 0 || ChieuRong < 0)
                {
                    check = false;
                    throw new ArgumentException();
                }
            }
            catch (Exception e)
            {
                check = false;
                Console.WriteLine("Lam On chi nhap so nguyen duong");
                _ChieuDai = int.Parse(Console.ReadLine());
                _ChieuRong = int.Parse(Console.ReadLine());
            }
        } while (check == false);
    }
    public override int CalculateArea()
    {
        return ChieuDai * ChieuRong;
    }

    public override int CalculatePerimeter()
    {
        return (ChieuDai + ChieuRong) * 2;
    }
}
