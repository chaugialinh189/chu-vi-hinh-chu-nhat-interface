﻿// See https://aka.ms/new-console-template for more information
public abstract class Shape<T>
{
    public abstract T CalculateArea();
    public abstract T CalculatePerimeter();

}
